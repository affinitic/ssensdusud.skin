from  ssensdusud.skin.testing import SSENSDUSUD_SKIN_FUNCTIONAL_TESTING
from plone.testing import layered
import robotsuite
import unittest


def test_suite():
    suite = unittest.TestSuite()
    suite.addTests([
        layered(robotsuite.RobotTestSuite("robot_test.txt"),
                layer=SSENSDUSUD_SKIN_FUNCTIONAL_TESTING)
    ])
    return suite