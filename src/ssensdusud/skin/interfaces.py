from zope.interface import Interface


class ISsensdusudTheme(Interface):
    """
    Marker interface that defines a ZTK browser layer.
    """
