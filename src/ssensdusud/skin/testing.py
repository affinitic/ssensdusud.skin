from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting

from plone.testing import z2

from zope.configuration import xmlconfig


class SsensdusudskinLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load ZCML
        import ssensdusud.skin
        xmlconfig.file(
            'configure.zcml',
            ssensdusud.skin,
            context=configurationContext
        )

        # Install products that use an old-style initialize() function
        #z2.installProduct(app, 'Products.PloneFormGen')

#    def tearDownZope(self, app):
#        # Uninstall products installed above
#        z2.uninstallProduct(app, 'Products.PloneFormGen')


SSENSDUSUD_SKIN_FIXTURE = SsensdusudskinLayer()
SSENSDUSUD_SKIN_INTEGRATION_TESTING = IntegrationTesting(
    bases=(SSENSDUSUD_SKIN_FIXTURE,),
    name="SsensdusudskinLayer:Integration"
)
SSENSDUSUD_SKIN_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(SSENSDUSUD_SKIN_FIXTURE, z2.ZSERVER_FIXTURE),
    name="SsensdusudskinLayer:Functional"
)
